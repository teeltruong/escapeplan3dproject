using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimator : MonoBehaviour
{
    [SerializeField] private Animator anim;

    public void BossDie()
    {
        anim.Play("GrenadierDeath");
    }
}
