using UnityEngine;

public class MonsterDropManager : MonoBehaviour
{
    [SerializeField] private CharacterID characterID;
    [SerializeField] private MonsterDropConfig monsterDropConfig;

    public GameObject GetPrefab()
    {
        var rate = Random.Range(1, 101);
        var monsterDropSetting = GetMonsterDropSetting();
        
        var prefab = GetPrefab(monsterDropSetting,rate).prefab;
        return prefab;
    }

    private Item GetPrefab(MonsterDropSetting monsterDropSetting,int rate)
    {
        var prefab = new Item();

        if (rate >= 80)
        {
            prefab = monsterDropSetting.healingPotion;
        }
        // else if (rate >= 60)
        // {
        //     prefab = monsterDropSetting.manaPotion;
        // }
        // else if (rate >= 40)
        // {
        //     prefab = monsterDropSetting.invinciblePotion;
        // }
        // else
        // {
        //     prefab = monsterDropSetting.questItem;
        // }
        
        return prefab;
    }
    
    private MonsterDropSetting GetMonsterDropSetting()
    {
        var currentSetting = new MonsterDropSetting();
        switch (characterID)
        {
            case CharacterID.Ghoul:
                currentSetting = monsterDropConfig.ghoul;
                break;
            case CharacterID.GhoulBoss:
                currentSetting = monsterDropConfig.ghoulBoss;
                break;
            case CharacterID.GhoulFestering:
                currentSetting = monsterDropConfig.ghoulFestering;
                break;
            case CharacterID.GhoulGrotesque:
                currentSetting = monsterDropConfig.ghoulGrotesque;
                break;
            case CharacterID.GhoulScavenger:
                currentSetting = monsterDropConfig.ghoulScavenger;
                break;
        }

        return currentSetting;
    }
}
