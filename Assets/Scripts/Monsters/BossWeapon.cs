using System;
using UnityEngine;

public class BossWeapon : MonoBehaviour
{
    [SerializeField] private float damage;

    private void OnCollisionEnter(Collision collision)
    {
        var targetCharacter = collision.gameObject.GetComponent<Character>();
        if (!targetCharacter) return;
        if (targetCharacter.Team == Team.Monster) return;

        targetCharacter.TakeDamage(damage);
    }

    private void OnTriggerEnter(Collider other)
    {
        var targetCharacter = other.GetComponent<Character>();
        if (!targetCharacter) return;
        if (targetCharacter.Team == Team.Monster) return;

        targetCharacter.TakeDamage(damage);
    }
}
