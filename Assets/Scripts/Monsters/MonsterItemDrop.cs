using UnityEngine;

public class MonsterItemDrop : MonoBehaviour
{
    [SerializeField] private MonsterDropManager monsterDropManager;
    [SerializeField] private Transform dropPoint;

    public void Setup() => Instantiate(monsterDropManager.GetPrefab(), dropPoint.position, dropPoint.rotation);
}
