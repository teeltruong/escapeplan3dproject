using System;

[Serializable]
public class MonsterDropSetting
{
    public Item healingPotion;
    public Item manaPotion;
    public Item invinciblePotion;
    public Item questItem;
}
