using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Handles interaction with the Enemy */

public class Enemy : MonoBehaviour
{
    [SerializeField] private EnemyName enemyName;
   
}

[Serializable]
public enum EnemyName
{
    Ghoul,
    GhoulFestering,
    GhoulScavenger,
    GhoulGrotesque,
    GhoulBoss,
}