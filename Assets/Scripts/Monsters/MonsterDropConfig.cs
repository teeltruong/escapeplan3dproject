using UnityEngine;

[CreateAssetMenu(menuName = "MonsterDropConfig/MonsterDropConfig", fileName = nameof(MonsterDropConfig))]
public class MonsterDropConfig : ScriptableObject
{
    public MonsterDropSetting ghoul;
    public MonsterDropSetting ghoulFestering;
    public MonsterDropSetting ghoulScavenger;
    public MonsterDropSetting ghoulGrotesque;
    public MonsterDropSetting ghoulBoss;
}
