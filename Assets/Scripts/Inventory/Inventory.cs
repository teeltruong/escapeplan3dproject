using System;
using System.Collections.Generic;
using System.Linq;
using TigerForge;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    [SerializeField] private List<Item> inventory;
    [SerializeField] private List<Image> bags;
    public static Inventory Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void UseHealthPotion()
    {
        if (inventory.Any(item => item.itemName == ItemName.HealPotion))
        {
            for (var i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].itemName != ItemName.HealPotion) continue;
                inventory.RemoveAt(i);
                PlayerManager.instance.UseHealthPotion(500);
                UpdateView(ItemName.HealPotion);
                return;
            }
        }
    }
    
    public void UseManaPotion()
    {
        if (inventory.Any(item => item.itemName == ItemName.ManaPotion))
        {
            for (var i = 0; i < inventory.Count;i++)
            {
                if (inventory[i].itemName != ItemName.ManaPotion) continue;
                inventory.RemoveAt(i);
                return;
            }
           
            UpdateView(ItemName.ManaPotion);
        }
    }
    
    public void UseInvinciblePotion()
    {
        if (inventory.Any(item => item.itemName == ItemName.Invincible))
        {
            for (var i = 0; i < inventory.Count;i++)
            {
                if (inventory[i].itemName != ItemName.Invincible) continue;
                inventory.RemoveAt(i);
                return;
            }
           
            UpdateView(ItemName.Invincible);
        }
    }
    
    public void AddToInventory(Item item)
    {
        inventory.Add(item);
        AddHealingPotion();
        AddManaPotion();
        AddInvinciblePotion();
        AddToBag();
    }

    private void AddToBag()
    {
        for (var i = 0; i < inventory.Count; i++)
        {
            for (var j = 0; j < bags.Count; j++)
            {
                if (i == j)
                {
                    bags[j].sprite = inventory[i].sprite;
                    bags[j].gameObject.SetActive(true);
                }
            }
        }
    }
    
    private void AddHealingPotion()
    {
        if(inventory.Any(item => item.itemName == ItemName.HealPotion)) UpdateView(ItemName.HealPotion);
    }
    
    private void AddManaPotion()
    {
        if(inventory.Any(item => item.itemName == ItemName.ManaPotion))  UpdateView(ItemName.ManaPotion);
    }
    
    private void AddInvinciblePotion()
    {
        if (inventory.Any(item => item.itemName == ItemName.Invincible)) UpdateView(ItemName.Invincible);
    }

    private void UpdateView(ItemName itemName)
    {
        EventManager.EmitEventData(Player.Select.PickupPotion,new PotionInfo
        {
            potionType = (PotionType)itemName,
            sprite = GetSprite(itemName),
            count = GetItemCount((QuestName)itemName)
        });
    }
    
    public int GetItemCount(QuestName itemName)
    {
        return inventory.Count(items => items.questName == itemName);
    }
    
    private Sprite GetSprite(ItemName itemName)
    {
        var item = inventory.Find(item => item.itemName == itemName);
        return item.sprite;
    }
}

[Serializable]
public class PotionInfo
{
    public PotionType potionType;
    public Sprite sprite;
    public int count;
}