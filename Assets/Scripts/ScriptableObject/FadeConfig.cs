using System;
using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(menuName = "DoTween/FadeConfig", fileName = nameof(FadeConfig))]
public class FadeConfig : ScriptableObject
{
    public FadeSetting items;
}

[Serializable]
public class FadeSetting
{
    public float fade;
    public float normal;
    public float duration;
    public Ease ease;
}
