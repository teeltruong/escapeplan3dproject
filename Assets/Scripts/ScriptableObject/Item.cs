using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(menuName = "Item/Item", fileName = nameof(Item))]
    public class Item : ScriptableObject
    {
        public ItemName itemName;
        public QuestName questName;
        public Sprite sprite;
        public int quantity;
        public GameObject prefab;
    }

