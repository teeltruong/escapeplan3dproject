using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewQuest : MonoBehaviour
{
    [SerializeField] private GameObject questPrefab;
    [SerializeField] private Transform questParent;
    [SerializeField] private TMP_Text questDetail;
    [SerializeField] private Button button;
    
    private Quest selectedQuest;
    public static NewQuest Instance;
    public List<Quest> quests = new List<Quest>();
    
    private void Awake()
    {
        Instance = this;
        button.onClick.AddListener(Setup);
    }

    private void Setup() => QuestGiver.Instance.OnAccepted(selectedQuest);
    
    public void AcceptQuest(Quest quest)
    {
        quests.Add(quest);
        var go = Instantiate(questPrefab, questParent);

        var qs = go.GetComponent<QuestScript>();
        quest.QuestScript = qs;
        qs.CurrentQuest = quest;

        go.GetComponent<TMP_Text>().text = quest.Title;
    }

    public void ShowDetail(Quest quest)
    {
        selectedQuest = quest;
        var objective = "\nObjectives\n";
        
        objective += $"{quest.CollectObjectives.Type}: {quest.CollectObjectives.CurrentAmount}/{quest.CollectObjectives.Amount}";

        questDetail.SetText(string.Format($"<b>{quest.Title}</b>\n\t{quest.Detail}\n\t{objective}"));
    }
}
