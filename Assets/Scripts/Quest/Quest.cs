using System;
using System.Collections.Generic;
using TigerForge;
using UnityEngine;

[Serializable]
public class Quest
{
    [SerializeField] private int id;
    [SerializeField] private string title;
    [SerializeField] private string detail;

    [SerializeField] private CollectObjective collectObjective; 
    public QuestScript QuestScript { get; set; }
    
    public int Id
    {
        get => id;
        set => id = value;
    }
    
    public string Title
    {
        get => title;
        set => title = value;
    }
    
    public string Detail
    {
        get => detail;
        set => detail = value;
    }
    
    public CollectObjective CollectObjectives
    {
        get => collectObjective;
        set => collectObjective = value;
    }
}

