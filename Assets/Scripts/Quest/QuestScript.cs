using System.Linq;
using UnityEngine;

public class QuestScript : MonoBehaviour
{
    public Quest CurrentQuest { get; set; }

    public void SelectQuest()
    {
        NewQuest.Instance.ShowDetail(CurrentQuest);
        foreach (var quest in QuestLog.Instance.quests.Where(quest => quest.Id == CurrentQuest.Id))
        {
            QuestLog.Instance.ShowDetail(CurrentQuest);
        }
    }

}
