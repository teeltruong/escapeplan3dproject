using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CompletedQuest : MonoBehaviour
{
    [SerializeField] private List<Quest> quests;

    public static CompletedQuest Instance;

    private void Start()
    {
        Instance = this;
    }

    public void AddCompletedQuest(Quest quest)
    {
        quests.Add(quest);
    }

    public bool FindQuest(int id)
    {
        return quests.Any(quest => quest.Id == id);
    }
}
