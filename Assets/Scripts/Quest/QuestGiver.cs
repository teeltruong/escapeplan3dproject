using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TigerForge;
using UnityEngine;

public class QuestGiver : MonoBehaviour
{
    public List<Quest> quests;

    // [SerializeField] private NewQuest newQuest;
    [SerializeField] private QuestLog questLog;

    public static QuestGiver Instance;

    private void Awake()
    {
        Instance = this;
        UpdateDefault();
    }


    private void UpdateDefault()
    {
        foreach (var quest in quests)
        {
            questLog.AcceptQuest(quest);
        }
    }
    private void OnEnable()
    {
        EventManager.StartListening(Player.Select.UpdateQuest,UpdateView);
        UpdateView();
    }
    private void OnDisable()
    {
        EventManager.StopListening(Player.Select.UpdateQuest,UpdateView);
    }

    private void UpdateView()
    {
        var nullableData = EventManager.GetData(Player.Select.UpdateQuest);
        if (nullableData is Item item)
        {
            foreach (var quest in quests)
            {
                quest.CollectObjectives.UpdateQuestCount(item);
            }
        }
    }
    
    public void OnCompleted(Quest quest)
    {
        for (var i = 0; i < quests.Count; i++)
        {
            if (quests[i].Id == quest.Id)
            {
                Destroy(quests[i].QuestScript.gameObject);
                questLog.completeButton.interactable = false;
                break;
            }
        }
    }
    
    public void OnAccepted(Quest quest)
    {
        for (var i = 0; i < quests.Count; i++)
        {
            if (quests[i].Id == quest.Id)
            {
                Destroy(quests[i].QuestScript.gameObject);
                questLog.AcceptQuest(quest);
                break;
            }
        }
    }
    
    public void OnAbandoned(Quest quest)
    {
        questLog.AcceptQuest(quest);
        quests.Add(quest);
    }
}
