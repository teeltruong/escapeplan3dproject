using System;
using System.Collections;
using System.Collections.Generic;
using TigerForge;
using UnityEngine;

[Serializable]
public class CollectObjective
{
    [SerializeField] private int amount;
    private int currentAmount;
    [SerializeField] private QuestName type;
    
    public QuestName Type
    {
        get => type;
        set => type = value;
    }
    
    public int Amount
    {
        get => amount;
        set => amount = value;
    }
    
    public int CurrentAmount
    {
        get => currentAmount;
        set => currentAmount = value;
    }
    
    public void UpdateQuestCount(Item item)
    {
        if (Type == item.questName)
        {
            CurrentAmount = Inventory.Instance.GetItemCount(Type);
        }
    }
}
