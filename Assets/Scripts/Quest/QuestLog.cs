using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestLog : MonoBehaviour
{
    [SerializeField] private GameObject questPrefab;
    [SerializeField] private Transform questParent;
    [SerializeField] private TMP_Text questDetail;
    public Button completeButton;
    public Button abandonButton;
    
    private Quest selectedQuest;
    private Quest completedQuest;
    public static QuestLog Instance;
    public List<Quest> quests = new List<Quest>();

    private void Awake()
    {
        Instance = this;
        completeButton.onClick.AddListener(OnComplete);
        abandonButton.onClick.AddListener(OnAbandon);
    }
    
    private void OnComplete()
    {
        CompletedQuest.Instance.AddCompletedQuest(completedQuest);
        QuestGiver.Instance.OnCompleted(completedQuest);
    }
    
    private void OnAbandon()
    {
        foreach (var quest in quests.Where(quest => quest.Id == selectedQuest.Id))
        {
            Destroy(quest.QuestScript.gameObject);
            //quest.QuestScript.gameObject.SetActive(false);
        }
        QuestGiver.Instance.OnAbandoned(selectedQuest);
    }

    public void AcceptQuest(Quest quest)
    {
        quests.Add(quest);
        var go = Instantiate(questPrefab, questParent);

        var qs = go.GetComponent<QuestScript>();
        quest.QuestScript = qs;
        qs.CurrentQuest = quest;

        go.GetComponent<TMP_Text>().text = quest.Title;
    }


    public void ShowDetail(Quest quest)
    {
        var isQuestDone = quest.CollectObjectives.CurrentAmount >= quest.CollectObjectives.Amount;
        if(isQuestDone) completedQuest = quest;
        completeButton.interactable = isQuestDone;
        
        selectedQuest = quest;
        var objective = "\nObjectives\n";
        
        objective += $"{quest.CollectObjectives.Type}: {quest.CollectObjectives.CurrentAmount}/{quest.CollectObjectives.Amount}";

        questDetail.SetText(string.Format($"<b>{quest.Title}</b>\n\t{quest.Detail}\n\t{objective}"));
    }
}
