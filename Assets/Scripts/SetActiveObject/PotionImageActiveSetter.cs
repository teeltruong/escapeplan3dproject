using System.Collections;
using System.Collections.Generic;
using TigerForge;
using UnityEngine;
using UnityEngine.UI;

public class PotionImageActiveSetter : MonoBehaviour
{
    [SerializeField] private PotionType potionType;
    [SerializeField] private GameObject image;
    [SerializeField] private Button baseImage;

    private void OnEnable()
    {
        EventManager.StartListening(Player.Select.PickupPotion, Setup);
        Setup();
    }

    private void OnDisable()
    {
        EventManager.StopListening(Player.Select.PickupPotion, Setup);
    }

    private void Setup()
    {
        var data = EventManager.GetData(Player.Select.PickupPotion);
        if (data is PotionInfo potionInfo)
        {
            if (potionType == potionInfo.potionType)
            {
                image.SetActive(!(potionInfo.count > 0));
                baseImage.image.enabled = ((potionInfo.count > 0));
            }
        }
    }
}
    
