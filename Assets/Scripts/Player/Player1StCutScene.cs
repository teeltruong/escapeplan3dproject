using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class Player1StCutScene : MonoBehaviour
{
    [SerializeField] private GameObject camera;
    [SerializeField] private GameObject mainCamera;
    [SerializeField] private Transform onBoardSpot;
    [SerializeField] private GameObject board;

    private void Start()
    {
        DeactivateCamera();
    }

    public void SetupPosition()
    {
        gameObject.transform.position = onBoardSpot.position;
        gameObject.transform.SetParent(board.transform);
    }
    public void DeactivateCamera()
    {
        camera.GetComponent<CinemachineBrain>().enabled = false;
        mainCamera.GetComponent<CinemachineBrain>().enabled = false;
    }
    
    public void ActivateCamera()
    {
        camera.GetComponent<CinemachineBrain>().enabled = true;
        mainCamera.GetComponent<CinemachineBrain>().enabled = true;
    }
}
