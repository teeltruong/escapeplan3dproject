using System;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerManager : MonoBehaviour {

    #region Singleton

    public static PlayerManager instance;

    void Awake ()
    {
        instance = this;
    }

    #endregion

    public GameObject player;
    public GameObject playerFoot;
    public Animator anim;
    
    public void UseHealthPotion(float value)
    {
        player.GetComponent<Character>().CurrentHealth += value;
    }
    
    public void UseManaPotion(float value)
    {
        player.GetComponent<Character>().CurrentMana += value;
    }
    
    public void IncreaseDamage(float value)
    {
        player.GetComponent<Character>().BaseDamage += value;
    }
    
    public void DecreaseDamage(float value)
    {
        player.GetComponent<Character>().BaseDamage -= value;
    }
    
    public void IncreaseHealthe(float value)
    {
        player.GetComponent<Character>().CurrentHealth += value;
    }

    public void OnPlayerDie()
    {
        anim.Play("EllenDeath");
        Invoke(nameof(DestroyPlayer),2f);
    }

    private void DestroyPlayer()
    {
        player.SetActive(false);
    }
    
    public void KillPlayer()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                MouseSetter(CursorLockMode.None, true);
            }
            else
            {
                MouseSetter(CursorLockMode.Locked, false);
                // Camera.main.
            }
            return;
        }

    }
    
    private void MouseSetter(CursorLockMode cursorMode, bool status)
    {
        Cursor.lockState = cursorMode;
        Cursor.visible = status;
    }
}