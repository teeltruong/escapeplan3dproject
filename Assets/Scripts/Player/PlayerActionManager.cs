using System;
using Gamekit3D;
using HutongGames.PlayMaker.Actions;
using TigerForge;
using UnityEngine;


public class PlayerActionManager : MonoBehaviour
{
    [SerializeField] private float delayHealth;
    [SerializeField] private float delayMana;
    [SerializeField] private float delayInvincible;

    [Space, SerializeField] private GameObject inventory;
    [SerializeField] private GameObject questLog;
    // [SerializeField] private GameObject newQuest;
    
    private bool isUseHealPotion;
    private bool isUseManaPotion;
    private bool isUseInvinciblePotion;
    private bool isQuestLogOpen;
    private bool isInventoryOpen;
    private bool isNewQuestOpen;
    private bool isPressU;
    private bool isPressI;

    private void Awake() => UpdateDefault();

    private void UpdateDefault()
    {
        MouseSetter(CursorLockMode.Locked,false);

        isUseHealPotion = true;
        isUseManaPotion = true;
        isUseInvinciblePotion = true;
        isPressU = true;
        isPressI = true;

        isQuestLogOpen = false;
        isInventoryOpen = false;
        isNewQuestOpen = false;
    }

    private void Start()
    {
        questLog.SetActive(false);
        inventory.SetActive(false);
        // newQuest.SetActive(false);
    }

    private bool btnEscape = false;
    private void Update()
    {
       
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            if (Cursor.lockState == CursorLockMode.None)
            {
                MouseSetter(CursorLockMode.Locked, false);
            }
            return;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (isUseHealPotion)
            {
                isUseHealPotion = false;
                UseHealthPotion();
            }
        }
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (isUseManaPotion)
            {
                isUseManaPotion = false;
                UseManaPotion();
            }
        }
        
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (isUseInvinciblePotion)
            {
                isUseInvinciblePotion = false;
                UseInvinciblePotion();
            }
        }
        
        if (Input.GetKeyDown(KeyCode.V))
        {
            inventory.SetActive(!inventory.activeSelf);
            UpdateMouseSetter(inventory.activeSelf);
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                MouseSetter(CursorLockMode.None, true);
            }
            else
            {
                MouseSetter(CursorLockMode.Locked, false);
            }
            return;
            
        }
        
        if (Input.GetKeyDown(KeyCode.G))
        {
            questLog.SetActive(!questLog.activeSelf);
            UpdateMouseSetter(questLog.activeSelf);
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                MouseSetter(CursorLockMode.None, true);
            }
            else
            {
                MouseSetter(CursorLockMode.Locked, false);
            }
            return;
        }
        // if (Input.GetKeyDown(KeyCode.G))
        // {
        //     newQuest.SetActive(!newQuest.activeSelf);
        //     UpdateMouseSetter(newQuest.activeSelf);
        // }
        //
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            EventManager.EmitEvent(Player.Select.UseKill1);
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            EventManager.EmitEvent(Player.Select.UseKill2);
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            EventManager.EmitEvent(Player.Select.UseKill3);
        }
        
        // Cheat
        if (Input.GetKeyDown(KeyCode.U))
        {
            if (isPressU)
            {
                isPressU = false;
                PlayerManager.instance.IncreaseDamage(500);
                Invoke(nameof(DelayIncreaseDamage),1f);
            }
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (isPressI)
            {
                isPressI = false;
                PlayerManager.instance.DecreaseDamage(500);
                Invoke(nameof(DelayDecreaseDamage),1f);
            }
        }
        
        if (Input.GetKeyDown(KeyCode.O))
        {
            if (isPressI)
            {
                isPressI = false;
                PlayerManager.instance.IncreaseHealthe(500);
                Invoke(nameof(DelayDecreaseDamage),1f);
            }
        }
    }

    private void UpdateMouseSetter(bool status)
    {
        switch (status)
        {
            case true:
                MouseSetter(CursorLockMode.Locked, true);
                break;
            case false:
                MouseSetter(CursorLockMode.None, false);
                break;
        }
    }

    private void MouseSetter(CursorLockMode cursorMode, bool status)
    {
        Cursor.lockState = cursorMode;
        Cursor.visible = status;
    }

    private void DelayIncreaseDamage()
    {
        isPressU = true;
    }
    
    private void DelayDecreaseDamage()
    {
        isPressI = true;
    }
    
    private void UseHealthPotion()
    {
        Inventory.Instance.UseHealthPotion();
        EventManager.EmitEventData(Player.Select.UseHealthPotion,delayHealth);

        Invoke(nameof(IsHealingRefresh),delayHealth);
    }
    
    private void UseManaPotion()
    {
        Inventory.Instance.UseManaPotion();
        EventManager.EmitEventData(Player.Select.UseManaPotion,delayMana);

        Invoke(nameof(IsManaRefresh),delayMana);
    }
    
    private void UseInvinciblePotion()
    {
        Inventory.Instance.UseInvinciblePotion();
        EventManager.EmitEventData(Player.Select.UseInvinciblePotion,delayInvincible);

        Invoke(nameof(IsInvincibleRefresh),delayInvincible);
    }
    
    private void IsHealingRefresh()
    {
        isUseHealPotion = true;
    }
    private void IsManaRefresh()
    {
        isUseManaPotion = true;
    }
    private void IsInvincibleRefresh()
    {
        isUseInvinciblePotion = true;
    }
}