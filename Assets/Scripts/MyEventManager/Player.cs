using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Player 
{
    public static class Select
    {
        public const string PickupPotion = "PickupPotion";
        public const string UseHealthPotion = "UseHealthPotion";
        public const string UseManaPotion = "UseManaPotion";
        public const string UseInvinciblePotion = "UseInvinciblePotion";
        public const string UseKill1 = "UseKill1";
        public const string UseKill2 = "UseKill2";
        public const string UseKill3 = "UseKill3";
        public const string UpdateQuest = "UpdateQuest";
        public const string AcceptQuest = "UpdateQuest";
    }
}
