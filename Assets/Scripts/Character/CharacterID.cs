using System;

[Serializable]
public enum CharacterID
{
    Elen = 1,
    
    
    Ghoul = 5,
    GhoulFestering = 6,
    GhoulScavenger = 7,
    GhoulGrotesque = 8,
    GhoulBoss = 9,
}

