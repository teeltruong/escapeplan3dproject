using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHealthBar : MonoBehaviour
{
    [SerializeField] private Image healthValue;
    [SerializeField] private Character character;
    
    private void OnEnable()
    {
        character.onHealthChanged += UpdateHealthValue;
        UpdateHealthValue();
    }

    private void OnDestroy() => character.onHealthChanged -= UpdateHealthValue;
    
    private void UpdateHealthValue() =>
        healthValue.DOFillAmount((int)(character.MaxHealth-character.CurrentHealth),character.CurrentHealth/character.MaxHealth,0.5f);
}
