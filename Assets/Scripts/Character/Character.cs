using System;
using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour
{
    [SerializeField] private Team team;
    [SerializeField] private float maxHealth;
    [SerializeField] private float currentHealth;
    [SerializeField] private float maxMana;
    [SerializeField] private float currentMana;
    [SerializeField] private float baseDamage;
    [SerializeField] private float defend;
    
    public Action onHealthChanged;
    public Action onManaChanged;
    public UnityEvent onDead;

    public float BaseDamage
    {
        get => baseDamage;
        set => baseDamage = value;
    }

    public float MaxHealth => maxHealth;
    public float MaxMana => maxMana;
    
    public Team Team => team;
    
    public float CurrentHealth
    {
        get => currentHealth;
        set
        {
            currentHealth = value;
            if (currentHealth > maxHealth) currentHealth = maxHealth;
            onHealthChanged?.Invoke();
        }
    }
    
    public float CurrentMana
    {
        get => currentMana;
        set
        {
            currentMana = value;
            onManaChanged?.Invoke();
        }
    }

    public bool IsDead => CurrentHealth <= 0;

    private void Start()
    {
        CurrentHealth = maxHealth;
        CurrentMana = maxMana;
    }

    public void TakeDamage(float damage)
    {
        // if (IsDead) return;

        CurrentHealth -= damage;
        Debug.Log(CurrentHealth);
        if (IsDead)
        {
            onDead.Invoke();
        }
    }
}
