using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CharacterManaBar : MonoBehaviour
{
    [SerializeField] private Image manaValue;
    [SerializeField] private Character character;
    
    private void OnEnable()
    {
        character.onManaChanged += UpdateManaValue;
        UpdateManaValue();
    }

    private void OnDestroy() => character.onManaChanged -= UpdateManaValue;
    
    private void UpdateManaValue() =>
        manaValue.DOFillAmount((int)(character.MaxMana-character.CurrentMana),character.CurrentMana/character.MaxMana,0.5f);
}
