using System.Collections;
using System.Collections.Generic;
using Invector.vCharacterController;
using UnityEngine;

public class SignalTestMessage : MonoBehaviour
{

    [SerializeField] private vThirdPersonController control;

    public void Freeze()
    {
        control.enabled = false;
    }

    public void Release()
    {
        control.enabled = true;
    }
}
