using System;



[Serializable]
public enum ItemName
{
    HealPotion = 1,
    ManaPotion = 2,
    Invincible = 3,
    Branch = 4,
    Ghoul = 5,
    GhoulFestering = 6,
    GhoulScavenger = 7,
    GhoulGrotesque = 8,
    GhoulBoss = 9,
    Paddle = 10,
    FinalQuest = 11
}

[Serializable]
public enum QuestName
{
    HealPotion = 1,
    ManaPotion = 2,
    Invincible = 3,
    Branch = 4,
    Ghoul = 5,
    GhoulFestering = 6,
    GhoulScavenger = 7,
    GhoulGrotesque = 8,
    GhoulBoss = 9,
    Paddle = 10,
    FinalQuest = 11
}
