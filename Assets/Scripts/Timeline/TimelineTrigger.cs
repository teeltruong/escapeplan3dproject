using UnityEngine;
using UnityEngine.Playables;

public class TimelineTrigger : MonoBehaviour
{
    public PlayableDirector director;
    
    private void OnTriggerEnter(Collider other)
    {
        var character = other.GetComponentInParent<Character>();
        var isQuestComplete = CompletedQuest.Instance.FindQuest(1);
        if (character != null && character.CompareTag("Player") && isQuestComplete)
            // if (character != null && character.CompareTag("Player"))

        {
            director.Play();
        }
    }
}
