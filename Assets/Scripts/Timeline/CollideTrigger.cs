using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CollideTrigger : MonoBehaviour
{
    public GameObject gameObject;
    
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger hit: " + other.name);
        var character = other.GetComponentInParent<Character>();
        if (character != null && character.CompareTag("Player"))
        {
            gameObject.SetActive(true);
            gameObject.GetComponent<Collider>().enabled = false;
            //Destroy(gameObject);
        }
    }
}
