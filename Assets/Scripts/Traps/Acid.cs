using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acid : MonoBehaviour
{
    [SerializeField] private float damage;
    
    private void OnTriggerStay(Collider other)
    {
        var targetCharacter = other.GetComponent<Character>();
        if (!targetCharacter) return;
        if (targetCharacter.Team == Team.Monster) return;

        targetCharacter.TakeDamage(damage);
    }
}
