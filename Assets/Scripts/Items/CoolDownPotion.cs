using System;
using System.Collections;
using DG.Tweening;
using TigerForge;
using UnityEngine;
using UnityEngine.UI;

public class CoolDownPotion : MonoBehaviour
{
    [SerializeField] private Image healValue;
    [SerializeField] private Image manaValue;
    [SerializeField] private Image invincibleValue;

    private bool isUseHealPotion;
    private bool isUseManaPotion;
    private bool isUseInvinciblePotion;
    
    private void Awake() => SetupDefault();
    
    private void OnEnable()
    {
        EventManager.StartListening(Player.Select.UseHealthPotion,SetupHeal);
        EventManager.StartListening(Player.Select.UseManaPotion,SetupMana);
        EventManager.StartListening(Player.Select.UseInvinciblePotion,SetupInvincible);
        SetupHeal();
        SetupMana();
        SetupInvincible();
    }

    
    private void OnDisable()
    {
        EventManager.StopListening(Player.Select.UseHealthPotion,SetupHeal);
        EventManager.StopListening(Player.Select.UseManaPotion,SetupMana);
        EventManager.StopListening(Player.Select.UseInvinciblePotion,SetupInvincible);
    }

    private void Update()
    {
        
    }

    private void SetupInvincible()
    {
        var data = EventManager.GetData(Player.Select.UseHealthPotion);
        if (data is float delay)
        {
            isUseInvinciblePotion = true;
            invincibleValue.enabled = true;

            if (isUseInvinciblePotion)
            {
                StopCoroutine(CountDown(delay, invincibleValue));
                StartCoroutine(CountDown(delay, invincibleValue));
                
                if (invincibleValue.fillAmount <= 0)
                {
                    invincibleValue.fillAmount = 1;
                    invincibleValue.enabled = false;
                }
                isUseInvinciblePotion = false;
            }
        }
    }

    private void SetupMana()
    {
        var data = EventManager.GetData(Player.Select.UseHealthPotion);
        if (data is float delay)
        {          
            isUseManaPotion = true;
            manaValue.enabled = true;

            if (isUseManaPotion)
            {
                StopCoroutine(CountDown(delay, manaValue));
                StartCoroutine(CountDown(delay, manaValue));
                
                if (manaValue.fillAmount <= 0)
                {
                    manaValue.fillAmount = 1;
                    manaValue.enabled = false;
                }
                isUseManaPotion = false;
            }
        }
    }

    private void SetupHeal()
    {
        
        var data = EventManager.GetData(Player.Select.UseHealthPotion);
        if (data is float delay)
        {
            isUseHealPotion = true;
            healValue.enabled = true;
            if (isUseHealPotion)
            {
                StopCoroutine(CountDown(delay, healValue));
                StartCoroutine(CountDown(delay, healValue));

                if (healValue.fillAmount <= 0)
                {
                    healValue.fillAmount = 1;
                    healValue.enabled = false;
                }
                isUseHealPotion = false;
            }
        }
    }

    private IEnumerator CountDown(float countDown,Image image)
    {
        var temp = countDown;
        var currentCount = countDown;
        image.fillAmount = 1;

        while (currentCount >= 0)
        {
            // image.fillAmount -= 1 / countDown;
            image.DOFillAmount((int)((temp - currentCount ) / temp) , ((currentCount - temp) / temp)  , countDown);
            currentCount--;
            yield return new WaitForSeconds(1);
        }
        image.fillAmount = 0;
    }
    
    private void SetupDefault()
    {
        healValue.enabled = false;
        manaValue.enabled = false;
        invincibleValue.enabled = false;
        isUseHealPotion = false;
        isUseManaPotion = false;
        isUseInvinciblePotion = false;
    }
}



[Serializable]
public class Potion
{
    public PotionType potionType;
    public float duration;
}