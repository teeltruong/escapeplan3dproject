using Other;
using TigerForge;
using UnityEngine;
using UnityEngine.UI;

namespace Items
{
    public class Items : MonoBehaviour
    {
        [SerializeField] private Item item;
        [SerializeField] private Image image;
        [SerializeField] private FadeConfig fadeConfig;
        
        private void OnTriggerEnter(Collider other)
        {
            var targetCharacter = other.GetComponent<Character>();
            if (!targetCharacter) return;
            if (targetCharacter.Team == Team.Monster) return;

            Inventory.Instance.AddToInventory(item);
            CurrentGameObject.Instance.DisableCollider(gameObject.GetComponent<Collider>());
            StartCoroutine(CurrentGameObject.Instance.DestroyObject(gameObject, fadeConfig.items.duration));
            EventManager.EmitEventData(Player.Select.UpdateQuest,item);
            CurrentGameObject.Instance.ImageFade(image,fadeConfig.items);
        }
    }
}
