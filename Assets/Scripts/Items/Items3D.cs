using TigerForge;
using UnityEngine;

public class Items3D : MonoBehaviour
{
    [SerializeField] private Item item;
        
    private void OnTriggerEnter(Collider other)
    {
        var targetCharacter = other.GetComponent<Character>();
        if (!targetCharacter) return;
        if (targetCharacter.Team == Team.Monster) return;

        Inventory.Instance.AddToInventory(item);
        EventManager.EmitEventData(Player.Select.UpdateQuest,item);
        Destroy(gameObject);
    }
}
