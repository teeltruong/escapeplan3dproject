using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Other
{
    public class CurrentGameObject : MonoBehaviour
    {
        public static CurrentGameObject Instance;

        void Awake ()
        {
            Instance = this;
        }
        
        public IEnumerator DestroyObject(GameObject gameObject,float dutation)
        {
            yield return new WaitForSeconds(dutation);
            gameObject.SetActive(false);
        }

        public void DisableCollider(Collider collider)
        {
            collider.enabled = false;
        }
        
        public void ImageFade(Image image,FadeSetting fadeConfig)
        {
            image.DOFade(fadeConfig.fade, fadeConfig.duration).SetEase(fadeConfig.ease);
        }
    }
}
