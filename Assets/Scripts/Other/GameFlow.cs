using System;
using DG.Tweening;
using HutongGames.PlayMaker.Actions;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Playables;

public class GameFlow : MonoBehaviour
{
    [SerializeField] private GameObject winCanvas;
    [SerializeField] private GameObject loseCanvas;
    [SerializeField] private GameObject blackFader;
    [SerializeField] private GameObject boss;
    [SerializeField] private GameObject finalQuest;
    public PlayableDirector director;
    private bool isBossDie = false;

    private void Awake()
    {
        winCanvas.SetActive(false);
        loseCanvas.SetActive(false);
        finalQuest.SetActive(false);
    }

    private void OnEnable()
    {
        finalQuest.SetActive(false);
    }

    public void OnBossDie()
    {
        Instantiate(finalQuest, transform.position,quaternion.identity);
        Invoke(nameof(BossDie),9f);
    }
    
    public void OnPlayerDie()
    {
        PlayerManager.instance.OnPlayerDie();
        blackFader.SetActive(false);
        Invoke(nameof(OnLose),3f);
    }

    private void BossDie()
    {
        // boss.SetActive(false);
        finalQuest.SetActive(true);
        finalQuest.transform.DOMoveY(finalQuest.transform.position.y+1, 1f);
    }


    private void OnWin()
    {
        winCanvas.SetActive(true);
        blackFader.SetActive(true);
    }

    private void OnLose()
    {
        loseCanvas.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        var targetCharacter = other.GetComponent<Character>();
        if (!targetCharacter) return;
        if (targetCharacter.Team == Team.Monster) return;
        
        if (CompletedQuest.Instance.FindQuest(1) && CompletedQuest.Instance.FindQuest(2))
        {
            director.Play();
            Invoke(nameof(OnWin), 20f);
        }
    }
}
