using System;
using TigerForge;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PotionManager : MonoBehaviour
{
    [SerializeField] private PotionType potionType;
    [SerializeField] private Image image;
    [SerializeField] private TMP_Text count;
    [SerializeField] private string textFormat = "{0}";
    [SerializeField] private Button baseImage;
    [SerializeField] private TMP_Text baseText;

    private void OnEnable()
    {
        EventManager.StartListening(Player.Select.PickupPotion,Setup);
        Setup();
    }
    
    private void OnDisable()
    {
        EventManager.StopListening(Player.Select.PickupPotion,Setup);
    }
    
    private void Setup()
    {
        var data = EventManager.GetData(Player.Select.PickupPotion);
        if (data is PotionInfo potionInfo)
        {
            UpdateView(potionInfo);
        }
    }
    
    private void UpdateView(PotionInfo potionInfo)
    {
        if (potionInfo.potionType != potionType) return;
        image.sprite = potionInfo.sprite;
        count.SetText(textFormat,potionInfo.count);
        
        var havePotion = potionInfo.count > 0;
        
        image.enabled = havePotion;
        baseImage.image.enabled = !havePotion;
        baseText.gameObject.SetActive(!havePotion);
    }
    
}

[Serializable]
public enum PotionType
{
    HealthPotion = 1,
    ManaPotion = 2,
    Invincible = 3
}