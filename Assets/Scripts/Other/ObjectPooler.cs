using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField] private List<Pool> pools;
    public Dictionary<EnemyName, Queue<GameObject>> poolDictionary;

    public static ObjectPooler Instance;

    private void Awake()
    {
        Instance = this;
        poolDictionary = new Dictionary<EnemyName, Queue<GameObject>>();

        foreach (var pool in pools)
        {
            var objectPool = new Queue<GameObject>();

            for (var index = 0; index < pool.size; index++)
            {
                var obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionary.Add(pool.enemyName,objectPool);
        }
    }
    
    public GameObject SpawnFromPool(EnemyName enemyName, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(enemyName))
        {
            return null;
        }
        var objectToSpawn = poolDictionary[enemyName].Dequeue();
        objectToSpawn.GetComponent<Character>().CurrentHealth = objectToSpawn.GetComponent<Character>().MaxHealth;
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;
        poolDictionary[enemyName].Enqueue(objectToSpawn);
        objectToSpawn.SetActive(true);
        return objectToSpawn;
    }
}

[Serializable]
public class Pool
{
    public EnemyName enemyName;
    public GameObject prefab;
    public int size;
}