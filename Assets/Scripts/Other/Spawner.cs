using System.Collections;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    [SerializeField] private EnemyName enemyName;
    [SerializeField] private float delaySpawn;
    [SerializeField] private int count;
    [SerializeField] private float offsetX;
    [SerializeField] private float offsetZ;
    private void OnTriggerEnter(Collider other)
    {
        var targetCharacter = other.GetComponent<Character>();
        if (!targetCharacter) return;
        if (targetCharacter.Team == Team.Monster) return;

        var position = transform.position;
        for (var i = 1; i <= count; i++)
        {
            ObjectPooler.Instance.SpawnFromPool(enemyName, new Vector3(Random.Range(position.x+offsetX,position.x-offsetX),position.y,Random.Range(position.z+offsetZ,position.z-offsetZ)), quaternion.identity);
        }
        gameObject.GetComponent<Collider>().enabled = false;
        Invoke(nameof(ActivateSpawner),delaySpawn);
    }

    private void ActivateSpawner() => gameObject.GetComponent<Collider>().enabled = true;
}
