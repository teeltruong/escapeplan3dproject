﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gamekit3D
{
    [DefaultExecutionOrder(9999)]
    public class FixedUpdateFollowDemo : MonoBehaviour
    {
        public Transform toFollow;
        public Transform fromFollow;

        private void FixedUpdate()
        {
            transform.position = toFollow.position;
            transform.rotation = toFollow.rotation;
            //
            // transform.position = fromFollow.position;
            // transform.rotation = fromFollow.rotation;
        }
    } 
}
